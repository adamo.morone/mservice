#!/usr/bin/python
class MessageQueue():

    messages = []

    @classmethod
    def is_empty(cls):
        return cls.messages == []

    @classmethod
    def push(cls, item):
        cls.messages.append(item)

    @classmethod
    def pop(cls):
        return cls.messages.pop()
    
    @classmethod
    def peek(cls):
        return cls.messages[len(cls.messages) - 1]
    
    @classmethod
    def size(cls):
        return len(cls.messages)