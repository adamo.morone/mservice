#!/usr/bin/python
import http

import threading

import time

from messagequeue import MessageQueue

from flask import Flask, request, jsonify

'''
    MESSAGE EXAMPLE
    {
        "origin": "SERVICE_A",
        "destiny": "SERVICE_B",
        "message": {
            "event": "EVENT_TOKEN",
            "content": " - ",
            "synchronous": false
        }
    }
'''

app = Flask(__name__)


def message_consumer():

    while True:
        if MessageQueue().size() > 0:
            app.logger.info("POP -> {0}".format(MessageQueue().pop()))
        time.sleep(2)


@app.route("/", methods=['POST'])
def get_message():

    app.logger.info("[{0}]".format(request.get_json()))

    try:
        '''
            Append message to queue and return a 202 message.
        '''

        MessageQueue().push(request.get_json()["message"])

        return jsonify({"code": http.HTTPStatus.ACCEPTED.numerator})
    except Exception:
        '''
            Return failure
        '''
        return jsonify({"code": http.HTTPStatus.INTERNAL_SERVER_ERROR.numerator})


def start_runner():
    thread = threading.Thread(target=message_consumer)
    thread.start()

if __name__ == "__main__":
    app.run()

start_runner()

